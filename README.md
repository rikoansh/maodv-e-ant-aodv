# MAODV-E-ANT-AODV

# Enhanced-Ant-AODV for optimal route selection in mobile ad-hoc network <br>

<b>Nama : Riko Anshori </b> <br>
<b>NRP : 05111850010040 </b> <br>
<b>Kelas : Audit Jaringan </b> <br>

<b>Konsep :</b><br>

- 1.Memodifikasi paket header RREQ dan RREP dengan menginput pheromone count 
- 2.Memodifikasi routing table dengan menginput RSSM (Received Signal Strenght Metric), REM (Residual Energy Metric), CM (Congestion Metric), HCM (Hop-Count Metric)
- 3.Menambahkan 2 threshold untuk membandingkan RSSM(SIGNAL_THR) dan REM (RE_THR) 

